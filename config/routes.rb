FantasyCongressLeague::Application.routes.draw do
  resource  :login, only: [:new, :create, :destroy], controller: :login
  resource  :login, only: [] do
    resources :password, only: [:new, :create, :show, :edit, :update], path_names: {edit: 'recover'}
  end
  resources :users, only: [:new, :create, :show]
  resource  :user, only: [:edit, :update], controller: :users
  resources :settings, only: [:edit, :update]
  resources :leagues do
    resources :teams, only: [:new, :create, :edit, :update, :show, :destroy] do
      resources :team_members, only: [:new, :create] do
        member do
          put :activate, action: :update, defaults: {rpc: 'activate'}
          put :leader, action: :update, defaults: {rpc: 'leader'}
          put :whip, action: :update, defaults: {rpc: 'whip'}
          post :recruit, action: :update, defaults: {rpc: 'recruit'}
          delete :vacate, action: :destroy, defaults: {rpc: 'vacate'}
          delete :recess, action: :destroy, defaults: {rpc: 'recess'}
          delete :retire, action: :destroy, defaults: {rpc: 'retire'}
        end
      end
      resources :tradings, only: [:create, :destroy]
      resources :activity, only: [:index, :show], controller: :annals
      resources :scoresheets, only: [:index]
    end
    member do
      post :authenticate
    end
  end
  get '/leagues/open/page/:id', to: 'leagues#index', as: 'leagues_open', defaults: {state: 'open'}
  get '/leagues/playing/page/:id', to: 'leagues#index', as: 'leagues_playing', defaults: {state: 'playing'}

  resources :legislators, only: [:show, :index] do
    resources :activity, only: [:index, :show], controller: :annals
  end
  get '/legislators/status/:status/page/:id', to: 'legislators#index', as: 'legislators_status'
  get '/legislators/party/:party/page/:id', to: 'legislators#index', as: 'legislators_party'
  get '/legislators/chamber/:chamber/page/:id', to: 'legislators#index', as: 'legislators_chamber'
  get '/legislators/gender/:gender/page/:id', to: 'legislators#index', as: 'legislators_gender'
  get '/legislators/name/:alpha/page/:id', to: 'legislators#index', as: 'legislators_name'
  get '/legislators/page/:id', to: 'legislators#index', as: 'legislators_page'
  get '/legislators/page/:id/:limit', to: 'bills#index', as: 'legislators_page_limit'

  resources :committees, only: [:index, :show]
  get '/committees/page/:id', to: 'committees#index', as: 'committees_page'
  get '/committees/page/:id/:limit', to: 'bills#index', as: 'committees_page_limit'

  resources :bills, only: [:index, :show]
  get '/bills/page/:id', to: 'bills#index', as: 'bills_page'
  get '/bills/page/:id/:limit', to: 'bills#index', as: 'bills_page_limit'
  get '/bills/chamber/:chamber/page/:id', to: 'bills#index', as: 'bills_chamber'
  get '/bills/status/:status/page/:id', to: 'bills#index', as: 'bills_status'
  get '/bills/docket/page/:id', to: 'bills#index', as: 'bills_docket', defaults: {docket: true}

  resource :capital_hill, only: [:show], controller: :capital_hill

  get  'help' => 'home#help'
  get  'statistics' => 'home#statistics'
  root to: 'home#show'
end