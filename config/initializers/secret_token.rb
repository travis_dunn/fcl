# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FantasyCongressLeague::Application.config.secret_key_base = '35af8b941b2c4db7d79d74d2c5854b94d697fd83bca96b9ac2d3f8169a1cf30cf28a05080029a7f4641743e87cc99ab966d26b007e49844370bbdac2ea9c3579'
