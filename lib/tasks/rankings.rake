namespace :rankings do
  desc "Rank teams by latest scores"
  task rank: :environment do
    UpdateRankingsJob.new.perform
  end
end