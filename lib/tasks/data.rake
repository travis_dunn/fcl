require "rsync"

namespace :data do
  desc "Download latest legislative data"
  task sync: :environment do
    UpdateDataJob.new.sync_files
  end 

  desc "Update legislative committees"
  namespace :committees do
    task update: :environment do
      UpdateCommitteesJob.new.perform
    end      
  end

  desc "Update legislators"
  namespace :legislators do
    task update: :environment do
      UpdateLegislatorsJob.new.perform
    end
  end 

  desc "Update attendance records"
  namespace :attendance do
    task update: :environment do
      UpdateAttendanceJob.new.perform
    end
  end 

  desc "Update bills and amendments"
  namespace :bills do
    task update: :environment do
      UpdateBillsJob.new.perform
    end
  end

  desc "Update congressional votes"
  namespace :votes do
    task update: :environment do
      # job_profiler = MethodProfiler.observe(UpdateVotesJob)
      UpdateVotesJob.new.perform
      # puts job_profiler.report
    end
  end  

  desc "Update congressional docket"
  namespace :docket do
    task update: :environment do
      UpdateDocketJob.new.perform
    end
  end    

  desc "Delete all player data"
  task reset: :environment do
    League.destroy_all
    Team.destroy_all
  end

  desc "Delete all legislative data"
  task destroy: :environment do
    Bill.destroy_all
    Amendment.destroy_all
    Vote.destroy_all
    Annal.destroy_all
    Scoresheet.destroy_all
    Docket.destroy_all
  end
end