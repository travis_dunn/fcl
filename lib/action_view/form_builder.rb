class FormBuilder < ActionView::Helpers::FormBuilder
  def initialize(object_name, object, template, options=nil, proc=nil)
    options ||= {}
    options[:html] ||= {}
    options[:html][:role] = 'form'
    options[:html][:novalidate] = 'novalidate'
    options[:html][:autocomplete] = false
    options[:html][:class] = form_class(options)
    super
  end

  def labeled_group(method, title=nil &block=nil)
    title ||= "#{method.to_s.titleize}:"
    @template.content_tag :div, class: 'form-group' do
      @template.concat @template.label @object_name, method, title, class: 'col-lg-2'
      tag = @template.content_tag :div, class: 'col-lg-5' do
        yield self
      end
      @template.concat tag
    end
  end

  def command_group(&block)
    @template.content_tag :div, class: 'form-group' do
      @template.content_tag :div, class: 'col-lg-offset-2 col-lg-10' do
        yield self
      end
    end
  end

  private

    def form_class(options)
      if options.fetch(:class,'') == 'inline-form'
      elsif options.fetch(:class)
        "#{options[:class]} form-horizontal"
      else
        'form-horizontal'
      end
    end
end