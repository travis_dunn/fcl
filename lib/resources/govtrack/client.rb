module GovTrack
  class Client
    include HTTParty

    base_uri "www.govtrack.us/api/v2"

    PAGE_LIMIT = 1000

    class << self
      def get_current_congress
      	res = query_url("/role", {current: true, limit: PAGE_LIMIT}).parsed_response
      	res.fetch('objects', [])
      end

      def get_congress_member(id)
      	res = query_url("/person/#{id}").parsed_response
      end

      def get_committees
        res = query_url("/committee", {obsolete: false, limit: PAGE_LIMIT}).parsed_response
        res.fetch('objects', [])
      end

      def get_committee_members
        objects = []
        res = get_committee_member_page
        meta = res.fetch('meta', {total_count: 0})
        objects = res.fetch('objects', [])

        page = 1
        while objects.size < meta['total_count'] do
          objects = objects + get_committee_member_page(page).fetch('objects', [])
          page = page + 1
        end 
        objects
      end      

      def get_bills(congress=113)
        objects = []
        res = get_bills_page(congress)
        meta = res.fetch('meta', {total_count: 0})
        objects = res.fetch('objects', [])

        page = 1
        while objects.size < meta['total_count'] do
          objects = objects + get_bills_page(congress, page).fetch('objects', [])
          page = page + 1
        end 
        objects
      end

      def get_bill(id)
        res = query_url("/bill/#{id}").parsed_response
      end

      def get_votes(congress=113)
        objects = []
        res = get_votes_page(congress)
        meta = res.fetch('meta', {total_count: 0})
        objects = res.fetch('objects', [])

        page = 1
        while objects.size < meta['total_count'] do
          objects = objects + get_votes_page(congress, page).fetch('objects', [])
          page = page + 1
        end 
        objects
      end

      def get_vote_voter(id)
        objects = []
        res = get_vote_voters_page(id, 0)

        meta = res.fetch('meta', {total_count: 0})
        objects = res.fetch('objects', [])

        page = 1
        while objects.size < meta['total_count'] do
          objects = objects + get_vote_voters_page(id, page).fetch('objects', [])
          page = page + 1
        end 
        objects
      end

      def get_roles
        objects = []
        res = get_roles_page
        meta = res.fetch('meta', {total_count: 0})
        objects = res.fetch('objects', [])

        page = 1
        while objects.size < meta['total_count'] do
          objects = objects + get_roles_page(page).fetch('objects', [])
          page = page + 1
        end 
        objects
      end

      private

        def get_committee_member_page(page=0)
          params = {limit: PAGE_LIMIT, offset: (page * PAGE_LIMIT)}
          query_url("/committee_member", params).parsed_response
        end

        def get_bills_page(congress, page=0)
          params = {congress: congress, limit: PAGE_LIMIT, offset: (page * PAGE_LIMIT)}
          query_url("/bill", params).parsed_response
        end

        def get_votes_page(congress, page=0)
          params = {congress: congress, limit: PAGE_LIMIT, offset: (page * PAGE_LIMIT)}
          query_url("/vote", params).parsed_response
        end  

        def get_vote_voters_page(vote_id, page=0)
          params = {vote: vote_id, limit: PAGE_LIMIT, offset: (page * PAGE_LIMIT)}
          query_url("/vote_voter", params).parsed_response
        end      

        def get_roles_page(page=0)
          params = {current: true, limit: PAGE_LIMIT, offset: (page * PAGE_LIMIT)}
          query_url("/role", params).parsed_response
        end    

        def query_url(url, query={})
          get url, query: query
        end
    end
  end
end