module OpenCongress
  class Client
    include HTTParty

    base_uri "http://api.opencongress.org"

    class << self
      def get_person(id)
      	query_url("/people", {person_id: id}).parsed_response
      end

      private

        def query_url(url, query={})
          get url, query: query
        end
    end
  end
end