class Time
  class << self
    def last(day)
      self.next(day, now - 1.week)
    end

    def next(day, from = nil)
      day = [:sunday,:monday,:tuesday,:wednesday,:thursday,:friday,:saturday].find_index(day) if day.class == Symbol
      one_day = 60 * 60 * 24
      original_date = from || now
      result = original_date
      result += one_day until result > original_date && result.wday == day 
      result.midnight
    end
  end

  def weekend?
    utc.wday == 0 || utc.wday == 6
  end
end
 