module AnnalsHelper
  def paginated_activity_path(page)
    if @parent.is_a?(Team)
      league_team_activity_path(@league, @team, page)
    elsif @parent.is_a?(Legislator)
      legislator_activity_path(@legislator, page)
    end
  end

  def link_to_activity(annal)
    if annal.parent_type == 'Bill'
      link_to annal.name, bill_path(annal.parent_id)
    elsif annal.parent_type == 'Amendment'
      link_to annal.name, bill_path(annal.parent.bill_id)
    end
  end
end