module ApplicationHelper
  def latest_docket
    @latest_docket ||= Docket.latest
  end

  def days_remaining_in_congress
    (Time.new(2015, 1, 3) - Time.zone.now).to_i / 1.day
  end

  def alphabet
    ("a".."z")
  end

  def button_bar_link_to(text, url, key, value=true)
    cls = params.fetch(key,nil) == value ? 'btn-primary' : 'btn-default'
    link_to text, url, class: "btn #{cls}"
  end  

  def office_label(legislator)
    cls = legislator.term.senator? ? 'label-primary' : 'label-info'
    content_tag :span, legislator.office, class: "label #{cls}"
  end
end
