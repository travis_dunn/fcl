module CommitteesHelper
  def committee_member_label(member)
    cls = member.chair? ? 'label-success' : (member.officer? ? 'label-info' : 'label-default')
    content_tag :span, member.title, class: "label #{cls}"
  end
end