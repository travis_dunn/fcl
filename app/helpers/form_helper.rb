module FormHelper
  def model_errors(model, post_only=true)
  	return if (post_only && request.get?) || (model.nil? || model.errors.empty?)

    content_tag :div, class: 'alert alert-danger' do
      content_tag :button, '&times;', type: 'button', class: 'close', data: {dismiss: 'alert'}
  	  content_tag :span, content_tag(:strong, "Error: ") << model.errors.full_messages.join('; ')
  	end
  end
end