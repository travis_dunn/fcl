module LeaguesHelper
  def waiting_on_league_drafting_team(league)
    link = link_to league.current_drafting_team.name, league_team_path(league, league.current_drafting_team)
    content_tag :em, "Waiting on #{link} to draft.".html_safe
  end

  def drafting_round(league)
    "Drafting round #{@league.draft_round+1}/#{@league.draft_rounds}"
  end
end