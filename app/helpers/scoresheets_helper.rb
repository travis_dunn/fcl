module ScoresheetsHelper
    def scoresheet_table(scoresheet)
      render partial: 'scoresheets/scoresheet', 
             locals: { scoresheet: scoresheet }
    end
end