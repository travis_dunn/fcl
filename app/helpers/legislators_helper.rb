module LegislatorsHelper
  def link_to_legislators_page_path(text, page)
    path = if params[:alpha]
      legislators_name_path(params[:alpha], page) 
    elsif params[:chamber]
      legislators_chamber_path params[:chamber], page
    elsif params[:party]
      legislators_party_path params[:party], page
    elsif params[:status]
      legislators_status_path params[:status], page
    else
      legislators_page_path page
    end
    link_to text.html_safe, path
  end
end