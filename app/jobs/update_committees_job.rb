class UpdateCommitteesJob < BaseJob
  include GovtrackSyncing
  
  def perform
    log "UpdateCommitteesJob updating committees"

    membership = YAML.load_file membership_path
    docs = YAML.load_file committees_path    

    docs.each do |doc|
      member = membership.fetch doc['thomas_id']
      committee = build_committee doc.merge!('membership' => member)
      committee.save
    end
  end

  private

    def build_committee(doc)
      committee = Committee.where(_id: doc['thomas_id'].downcase).first_or_initialize
      committee.name = doc['name']
      committee.category = doc['type']
      committee.url = doc['url']

      build_subcommittees committee, doc.fetch('subcommittees', [])
      build_membership committee, doc.fetch('membership', [])

      committee
    end

    def build_subcommittees(committee, subcommittees)
      subcommittees.each do |sc|
        subcommittee = build_subcommittee(sc)
        if committee.subcommittees.none?{|s| s.id == subcommittee.id }
          committee.subcommittees << subcommittee
        end
      end
    end

    def build_subcommittee(doc)
      sub = Subcommittee.new
      sub.name = doc['name']
      sub.id = doc['thomas_id']
      sub
    end

    def build_membership(committee, members)
      members.each do |m|
        member = build_member(m)
        if committee.members.none?{|l| l.id == member.id }
          committee.members << member
        end
      end
    end

    def build_member(doc)
      member = CommitteeMember.new
      member.title = doc['title'] unless doc['title'].blank?
      member.legislator = get_legislator doc['bioguide']
      member
    end    

    def get_legislator(id)
      @@legislators ||= Legislator.all
      @@legislators.detect{|l| l.id == id}
    end
end