class UpdateBillsJob < BaseJob
  include GovtrackSyncing
  
  def perform
    @legislators = Legislator.all.to_a.to_set
    @committees = Committee.all.to_a.to_set

    update_bills
    update_amendments
  end

  protected

    def update_bills
      log "UpdateBillsJob starting bill update"
      bcnt = Bill.count
      acnt = Annal.count

      time = Benchmark.realtime do
        bill_paths.each do |path|
          doc =  get_bill_doc path
          bill = build_bill(doc)
          bill.save
        end
      end

      log "Added #{(Bill.count - bcnt)} bills, #{(Annal.count - acnt)} annals (#{time} seconds)"
    end

    def update_amendments
      log "UpdateBillsJob starting amendment update"
      cnt = Amendment.count

      time = Benchmark.realtime do
        amendment_paths.each do |path|
          doc =  get_bill_doc path
          amendment = build_amendment(doc)
          
          doc['actions'].each do |a|
            action = build_action(a)
            if amendment.actions.select{|a| a.text == action.text && a.created_at == action.created_at }.blank?
              amendment.actions << action
            end
          end
          
          amendment.save
        end
      end

      log "Added #{(Amendment.count - cnt)} new amendments (#{time} seconds)"
    end

  private

    def get_bill_doc(path)
      load_json_doc(path)
    end

    def bill_doc_id(doc)
      [doc['congress'], doc['bill_type'], doc['number']].join
    end

    def amends_bill_doc_id(doc)
      doc['bill_id'].split('-').last << doc['bill_id'].split('-').first
    end

    def get_bill(id)
      Bill.where(_id: id).first_or_initialize
    end

    def build_bill(doc)
      bill = get_bill bill_doc_id(doc)

      bill.number = doc['number']
      bill.category = doc['bill_type']
      bill.congress = doc['congress']
      bill.official_title = doc['official_title']
      bill.congress = doc['congress']
      bill.introduced_at = doc['introduced_at']
      bill.updated_at = doc['status_at']
      bill.subjects = doc['subjects']
      bill.status = doc['status']
      bill.sponsor ||= build_sponsor doc['sponsor'].merge!(sponsored_at: doc['introduced_at'])
      build_cosponsors bill, doc.fetch('cosponsors', [])
      build_committees bill, doc.fetch('committees', [])
      build_actions bill, doc.fetch('actions', [])
      build_related_bills bill, doc.fetch('related_bills', [])

      bill
    end

    def build_amendment(doc)
      amendment = get_amendment doc['amendment_id']

      amendment.description = doc['description']
      amendment.purpose = doc['purpose']
      amendment.chamber = doc['chamber']
      amendment.number = doc['number']
      amendment.category = doc['amendment_type']
      amendment.status = doc['status']
      amendment.introduced_at = doc['introduced_at']
      amendment.updated_at = doc['updated_at']
      if doc['amends_bill']
        amendment.bill = get_bill amends_bill_doc_id(doc['amends_bill'])
      end
      if doc['amends_amendment']
        amendment.parent = get_amendment doc['amends_amendment']['amendment_id']
      end
      amendment.sponsor = build_sponsor(doc['sponsor'])
      amendment
    end    

    def build_cosponsors(bill, cosponsors)
      cosponsors.each do |cs|
        cosponsor = build_sponsor(cs)
        if cosponsor && bill.cosponsors.none?{|s| s.id == cosponsor.id }
          bill.cosponsors << cosponsor
        end
      end
    end

    def build_committees(bill, committees)
      committees.each do |c|
        referral = build_referral(c)
        if bill.referrals.none?{|r| r._id == referral._id }.blank?
          bill.referrals << referral
        end
      end
    end

    def build_actions(bill, actions)
      actions.each do |a|
        action = build_action(a)
        if bill.actions.none?{|a| a.text == action.text && a.created_at == action.created_at }
          bill.actions << action
        end
      end
    end

    def build_related_bills(bill, related)
      bill.related = Array.new unless related.blank?
      related.each do |r|
        bid = r['bill_id'].split('-').first + r['bill_id'].split('-').last
        bill.related << bid
      end
    end

    def build_referral(doc)
      referral = Referral.new
      referral.committee = get_committee doc['committee_id'].downcase
      referral.activity = doc['activity']
      referral.valid? ? referral : nil
    end

    def build_sponsor(doc)
      sponsor = Sponsor.new
      sponsor.legislator = get_legislator doc['thomas_id']
      sponsor.sponsored_at = doc['sponsored_at']
      sponsor.withdrawn_at = doc['withdrawn_at']
      sponsor.legislator ? sponsor : nil
    end    

    def build_action(doc)
      action = Action.new
      action.result = doc['result']
      action.status = doc['status']
      action.type = doc['type']
      action.text = doc['text']
      action.mechanism = doc['how']
      action.created_at = doc['acted_at']
      action
    end

    def get_legislator(thomas_id)
      @legislators.detect{|l| l.thomas_id == thomas_id}
    end

    def get_committee(thomas_id)
      @committees.detect{|c| c._id == thomas_id}
    end   

    def get_amendment(id)
      Amendment.where(_id: id).first_or_initialize
    end       
end