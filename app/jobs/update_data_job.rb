class UpdateDataJob < BaseJob
  include DelayedScheduling
  include GovtrackSyncing

  run_every 1.day

  SYNC_ARGS = ['-avz','--delete','--delete-excluded','--exclude **/text-versions/','--exclude **/*.xml']  

  def perform
    sync_files
    
    Delayed::Job.enqueue UpdateLegislatorsJob.new, priority: 1
    Delayed::Job.enqueue UpdateCommitteesJob.new, priority: 1, run_at: 5.minutes.from_now
    Delayed::Job.enqueue UpdateBillsJob.new, priority: 2, run_at: 10.minutes.from_now
    Delayed::Job.enqueue UpdateVotesJob.new, priority: 3, run_at: 30.minutes.from_now
  end

  def sync_files
    log "UpdateDataJob starting sync"

    time = Benchmark.realtime do
      file_urls.each do |source_path|
        Rsync.run(source_path, sync_path, SYNC_ARGS) do |result|
          if result.success?
            log("UpdateDataJob pulled changes from #{source_path}") unless result.changes.blank?
          else
            log "UpdateDataJob failed sync: #{result.error}"
            raise result.error
          end
        end
      end
    end
    log "UpdateDataJob finished sync (#{time} seconds)"
  end

  def max_attempts; 2; end

  private

    def file_urls
      [legislator_url, bill_url, amendment_url, vote_url]
    end
end
