class UpdateDocketJob < BaseJob
  include HTTParty
  include DelayedScheduling

  base_uri 'https://www.govtrack.us'

  run_every 1.week
  
  def perform
    log "Updating next week's docket"

    if docket = Docket.upcoming
      log "Docket already exists for #{docket.starts_at}-#{(docket.starts_at + 1.week)}"
      return
    end
    docket = Docket.new
    docket.scheduled_bill_ids = get_scheduled_bill_ids
    docket.sponsoring_legislator_ids = (docket.bills.select{|b| !b.sponsor.nil? }.map{ |b| b.sponsor.id } +
                                        docket.bills.map{ |b| b.cosponsor_ids }.flatten
                                       ).uniq
    docket.starts_at = Time.now.weekend? ? Time.next(:monday) : Time.last(:monday)
    log "Docket bills: #{docket.scheduled_bill_ids.size}"
    log "Docket legislators: #{docket.sponsoring_legislator_ids.size}"
    docket.save
  end

  private

    def get_govtrack_schedule_html
      self.class.get("/congress/bills/")
    end 

    def get_scheduled_bill_ids
      ids = []
      doc = Nokogiri::HTML(get_govtrack_schedule_html)
      doc.css('#docket a').each do |link|
        ids << get_bill_id_from_link(link['href'])
      end
      ids
    end

    def get_bill_id_from_link(url)
      congress = Rails.application.config.congress
      bill = url.split('/').last
      "#{congress}#{bill}"
    end
end