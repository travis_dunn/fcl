module GovtrackSyncing
  extend ActiveSupport::Concern

  protected

    def sync_path
      @sync_path ||= Rails.root.join('tmp', 'data')
    end

    def membership_path
      "#{sync_path}/congress-legislators/committee-membership-current.yaml"
    end

    def committees_path
      "#{sync_path}/congress-legislators/committees-current.yaml"
    end

    def legislators_path
      "#{sync_path}/congress-legislators/legislators-current.yaml"
    end

    def legislators_social_path
      "#{sync_path}/congress-legislators/legislators-social-media.yaml"
    end

    def bill_paths
      Dir["#{sync_path}/bills/**/data.json"]
    end

    def amendment_paths
      Dir["#{sync_path}/amendments/**/data.json"]
    end

    def vote_paths
      Dir["#{sync_path}/votes/**/data.json"]
    end    

    def legislator_url
      "govtrack.us::govtrackdata/congress-legislators"
    end

    def vote_url
      "govtrack.us::govtrackdata/congress/#{congress}/votes"
    end

    def amendment_url
      "govtrack.us::govtrackdata/congress/#{congress}/amendments"
    end

    def bill_url
      "govtrack.us::govtrackdata/congress/#{congress}/bills"
    end

    def photo_url
      "govtrack.us::govtrackdata/photos"
    end

  private

    def congress
      Rails.application.config.congress
    end
end