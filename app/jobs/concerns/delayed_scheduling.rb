module DelayedScheduling
  extend ActiveSupport::Concern

  def success(job)
    self.class.schedule!
  end

  def schedule!(run_at = nil)
    run_at ||= self.class.run_at
    Delayed::Job.enqueue self, priority: 0, run_at: run_at.utc
  end    

  module ClassMethods
    def run_every(time)
      @run_interval = time
    end

    def schedule(run_at = nil)
      schedule!(run_at) unless scheduled?
    end

    def schedule!(run_at = nil)
      new.schedule!(run_at)
    end
  
    def run_at
      run_interval.from_now
    end

    def scheduled_to_run_at
      if scheduled_run = handler_scope.pluck(:run_at).first
        scheduled_run
      else
        run_at
      end
    end

    protected

      def scheduled?
        handler_scope.exists?
      end

    private

      def handler_scope
        Delayed::Job.where(handler: handler_name_regex)
      end

      def handler_name_regex
        /^--- !ruby\/object:#{name}/i
      end

      def run_interval
        @run_interval ||= 1.hour
      end
  end
end