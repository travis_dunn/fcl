class BaseJob
  def perform
    raise NotImplementedError
  end

  def enqueue(job)
  end

  def success(job)
  end

  def error(job, exception)
  end

  def failure(job)
  end

  protected

    def log(text)
      Delayed::Worker.logger.add Logger::INFO, text
    end

    def load_json_doc(path)
      Oj.load File.read(path)
    end    
end