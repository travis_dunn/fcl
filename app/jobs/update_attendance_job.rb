class UpdateAttendanceJob < BaseJob
  include HTTParty
  include DelayedScheduling

  ATTENDANCE_PERIODS_LIMIT = 3

  base_uri 'https://www.govtrack.us'

  run_every 1.week
  
  def perform
    log "Updating legislator attendance"
    
    time = Benchmark.realtime do
      gov_track_ids.each do |id|
        doc = Nokogiri::HTML query_url(id).body

        rows = attendance_table_rows doc.css('#missedvotestable > tr')
        max = 0.0
        percent_column = 3
        rows.each do |row| 
          max = [max, row.css('td')[percent_column].inner_text.to_f].max
        end
        puts max
        attendance = 100.0 - max
        Legislator.where(gov_track_id: id).find_and_modify({"$set" => {attendance: attendance}})
      end
    end

    log "Updated attendance (#{time} seconds)"
  end

  private

    def gov_track_ids
      Legislator.where(gov_track_id: {'$ne'=>nil}).pluck(:gov_track_id)
    end

    def query_url(id)
      puts path_for_id(id)
      self.class.get path_for_id(id)
    end

    def path_for_id(id)
      "/congress/members/#{id}"
    end

    def attendance_table_rows(rows)
      from = [rows.size-ATTENDANCE_PERIODS_LIMIT - 1, 0].max
      to = rows.size - 1
      rows[from..to]
    end
end