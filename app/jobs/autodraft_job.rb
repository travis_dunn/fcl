class AutodraftJob < BaseJob
  run_every 1.hour

  BATCH_SIZE = 25
  
  def perform
    log "Autodrafting for Deadline"
    count = League.drafting_deadlined.count
    per_batch = 1000
    0.step(count, AutodraftJob::BATCH_SIZE) do |offset|
      leagues = League.limit(AutodraftJob::BATCH_SIZE).skip(offset).all
      
    end
    Team.rank!
  end
end