class UpdateRankingsJob < BaseJob
  include DelayedScheduling

  run_every 1.day
  
  def perform
    log "Ranking teams"
    Team.rank!
  end
end