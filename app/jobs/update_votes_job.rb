class UpdateVotesJob < BaseJob
  include GovtrackSyncing
  
  def perform
    @legislators = Legislator.all.to_a.to_set

    update_votes
  end

  private

    def update_votes
      log "UpdateVotesJob starting votes update"
      ccnt = Vote.count
      acnt = Annal.count

      time = Benchmark.realtime do
        vote_paths.each do |path|
          doc =  get_vote_doc(path)
          vote = build_vote(doc)
          vote.save
        end
      end

      log "Added #{(Vote.count - ccnt)} votes, #{(Annal.count - acnt)} annals (#{time} seconds)"
    end

    def get_vote_doc(path)
      load_json_doc(path)
    end    

    def vote_doc_id(doc)
      [doc['chamber'], doc['number'], doc['congress']].join
    end    

    def build_vote(doc)
      vote = Vote.where(_id: vote_doc_id(doc) ).first_or_initialize

      vote.number = doc['number']
      vote.chamber = doc['chamber']
      vote.congress = doc['congress']
      vote.name = doc['subject']
      vote.question = doc['question']
      vote.vote_type = doc['type']
      vote.category = doc['category']
      vote.session = doc['result']
      vote.created_at = doc['date']
      vote.requires = doc['requires']
      vote.result = doc['result']

      build_voting_record vote, doc
      vote
    end

    def build_voting_record(vote, doc)
      if vote.voting.empty?
        doc['votes'].fetch('Yea', []).each do |v|
          t = build_voting(v.merge!('choice' => 'yea'))
          vote.voting << t
        end
        doc['votes'].fetch('Nay', []).each do |v|
          t = build_voting(v.merge!('choice' => 'nay'))
          vote.voting << t
        end
        doc['votes'].fetch('Not Voting', []).each do |v|
          t = build_voting(v.merge!('choice' => 'absent'))
          vote.voting << t
        end
        doc['votes'].fetch('Present', []).each do |v|
          t = build_voting(v.merge!('choice' => 'abstain'))
          vote.voting << t
        end                       
      end
    end

    def build_voting(doc)
      if legislator = get_legislator(doc['id'])
        voting = Voting.new _id: legislator.id
        voting.choice = doc['choice'] 
        voting.party = doc['party']
        voting
      end
    end

    def get_legislator(bioguide_id)
      @legislators.detect{|l| l._id == bioguide_id }
    end
end