class UpdateLegislatorsJob < BaseJob
  include GovtrackSyncing

  attr_accessor :social_docs
  
  def perform
    log "Updating legislators"
    cnt = Legislator.count
    drt = 0

    docs = YAML.load_file legislators_path
    self.social_docs = YAML.load_file legislators_social_path

    time = Benchmark.realtime do
      docs.each do |doc|
        merge_social_media doc
        legislator = build_legislator(doc)
        drt++ if legislator.dirty?
        legislator.save
      end
    end

    log "Added #{(Legislator.count - cnt)} legislators, updated #{drt} legislators (#{time} seconds)"
  end

  private

    def merge_social_media(doc)
      if social_doc = get_social_doc(doc['id']['bioguide'])
        doc['social'] = social_doc['social']
      end
    end

    def get_social_doc(bioguide_id)
      self.social_docs.detect{|d| d['id']['bioguide'] == bioguide_id }
    end

    def build_legislator(doc)
      legislator = get_legislator doc['id']['bioguide']
      legislator.cspan_id = doc['id']['cspan']
      legislator.open_secrets_id = doc['id']['opensecrets']
      legislator.gov_track_id = doc['id']['govtrack']
      legislator.thomas_id = doc['id']['thomas']
      legislator.first_name = doc['name']['first']
      legislator.middle_name = doc['name']['middle']
      legislator.last_name = doc['name']['last']
      legislator.suffix = doc['name']['suffix']
      legislator.gender = doc['bio']['gender']
      legislator.religion = doc['bio']['religion']
      legislator.birthday = doc['bio']['birthday']

      doc['terms'].each do |t|
        term = build_term(t)
        unless legislator.terms.detect{|t| t.starts_at == term.starts_at }
          legislator.terms << term
        end
      end
      legislator.terms.sort!{|x,y| x <=> y }

      if doc.has_key?('social')
        legislator.twitter_id = doc['social']['twitter']
        legislator.facebook_id = doc['social']['facebook_id']
        legislator.youtube_id = doc['social']['youtube_id']
      end
      legislator
    end

    def get_legislator(bioguide_id)
      @@legislators ||= Legislator.all
      @@legislators.detect{|l| l.id == bioguide_id} || Legislator.new(_id: bioguide_id)
    end

    def build_term(doc)
      term = Term.new
      term.type = doc['type']
      term.party = doc['party']
      term.phone = doc['phone']
      term.address = doc['address']
      term.state = doc['state']
      term.state_rank = doc['state_rank']
      term.senate_class = doc['class']
      term.district = doc['district']
      term.starts_at = doc['start']
      term.ends_at = doc['end']
      term
    end
end