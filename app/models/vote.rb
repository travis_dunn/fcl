class Vote
  include Mongoid::Document

  field :name, type: String
  field :question, type: String
  field :vote_type, type: String
  field :category, type: String
  field :chamber, type: String
  field :requires, type: String
  field :congress, type: Integer, default: 113
  field :number, type: Integer
  field :session, type: Integer
  field :created_at, type: Time
  field :result, type: String

  belongs_to :bill
  belongs_to :amendment

  embeds_many :voting, class_name: 'Voting', cascade_callbacks: true

  validates :vote_type, presence: true
  validates :category, presence: true

  after_create :update_tracking

  class << self
    def updated_at
      sort(created_at:-1).pluck(:created_at)
    end
  end

  def passed?; result.match(/passed/i); end
  def failed?; result.match(/failed/i); end

  def on_pasage?; question.match(/on passage/i); end
  def on_cloture?; category == 'cloture'; end
  def on_amendment?; category == 'amendment'; end
  def on_bill_passage?; vote_type == 'On Passage of the Bill'; end

  def voting_against
    voting.select{|v| v.nay? }
  end

  def voting_in_favor
    voting.select{|v| v.yea? }
  end

  def voting_maverick
    voting.select{|v| party_choice(v.party) != v.choice }
  end  

  def voting_winners
    if passed?
      voting_in_favor
    elsif failed?
      voting_against
    else
      []
    end
  end

  def voting_absences
    voting.select{|v| v.absent? }
  end  

  def party_choice(party)
    yea = 0
    nay = 0
    voting.each do |v|
      if v.yea? 
        yea = yea +1
      elsif v.nay?
        nay = nay +1
      end
    end
    if yea > nay
      'yea'
    elsif nay > yea
      'nay'
    end
  end

  private

    def update_tracking
      against = voting_against
      absences = voting_absences

      annals = []

      if failed? && on_pasage?
        against_ids = against.map(&:_id)
        against_ids.each do |legislator_id|
          a = Hash.new parent: bill, legislator_id: legislator_id, category: 'defeated_bill', created_at: created_at
          annals << a
        end
        Scoresheet.count! against_ids, :defeated_bills, created_at
      end

      if against.size > 0
        maverick_ids = voting_maverick.map(&:_id)
        maverick_ids.each do |legislator_id|
          a = Hash.new parent: self, legislator_id: legislator_id, category: 'maverick_vote', created_at: created_at
          annals << a
        end
        Scoresheet.count! maverick_ids, :maverick_votes, created_at
      end

      if on_cloture?
        cat = passed? ? 'invoked_cloture' : 'rejected_cloture'
        winning_ids = voting_winners.map(&:_id)
        winning_ids.each do |legislator_id|
          a = Hash.new parent: self, legislator_id: legislator_id, category: cat, created_at: created_at
          annals << a
        end
        Scoresheet.count! winning_ids, :winning_cloture_votes, created_at
      end

      if on_amendment? && passed?
        Scoresheet.count! amendment.sponsor.id, :passed_amendments, created_at
      end

      if on_bill_passage? && bill && passed?
        Scoresheet.count! bill.sponsor.id, :sponsored_enacted_bills, created_at if bill.sponsor
        Scoresheet.count! bill.cosponsor_ids, :cosponsored_enacted_bills, created_at
      end

      if !absences.empty?
        absent_ids = absences.map(&:_id)
        Scoresheet.count! absent_ids, :absences, created_at
      end

      Annal.collection.insert annals unless annals.empty?
    end
end