class Action
  include Mongoid::Document

  field :result, type: String
  field :status, type: String
  field :type, type: String
  field :text, type: String
  field :mechanism, type: String
  field :created_at, type: Date

  embedded_in :legislation, polymorphic: true

  after_create :annal

  private

    def on_amendment?; legislation.is_a?(Amendment); end
    def on_bill?; legislation.is_a?(Bill); end

    def annal
      bill = on_bill? ? legislation : legislation.bill

      if category = annal_category && legislation.sponsor
        a = Annal.where(parent_id: bill._id, parent_type: 'Bill', legislator: legislation.sponsor.id, category: category).first_or_initialize
        a.created_at = created_at
        a.save
      end

      if on_bill? && category = annal_category(true)
        legislation.cosponsors.each do |sponsor|
          a = Annal.where(parent_id: bill._id, parent_type: 'Bill', legislator: sponsor.id, category: category).first_or_initialize
          a.created_at = created_at
          a.save
        end
      end
    end

    def annal_category(cosponsor=false)
      if on_bill? && status && status.match(/REFERRED/i)
      	'sponsored_referred_legislation'
      elsif on_bill? && status && status.match(/PASSED:BILL/i)
      	'sponsored_passed_legislation'
      elsif on_bill? && status && status.match(/CONFERENCE:PASSED/i)
      	'sponsored_approved_legislation'
      elsif on_bill? && status && status.match(/ENACTED:SIGNED/i) && cosponsor
        'cosponsored_enacted_legislation'
      elsif on_bill? && status && status.match(/ENACTED:SIGNED/i)
      	'sponsored_enacted_legislation'
      elsif on_amendment? && result && result.match(/pass/)
        'passed_amendment'
      end
    end
end