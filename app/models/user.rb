class User
  include Mongoid::Document
  include ActiveModel::SecurePassword 

  field :username, type: String
  field :email, type: String
  field :password_digest, type: String
  field :login_token, type: String
  field :created_at, :type => Time, :default => lambda { Time.now.utc }
  validates :username, presence: true, length: {minimum: 4, maximum: 64}
  validates :email, presence: true, length: {maximum: 256}

  has_secure_password

  has_many :leagues, as: :commissioner
  has_many :teams

  class << self
    def find_by_email(email)
      where(email: email.downcase).first
    end

    def login(email, password)
      user = find_by_email(email)
      user && user.authenticate(password)
    end

    def registration(params)
      user = self.new(params)
      user.password_confirmation = user.password
      user
    end

  	def remember(token)
      where(login_token: token).first
  	end

    def recover(token); remember(token); end
  end

  def generate_login_token!
    update_attribute :login_token, User.new.id
  end
end