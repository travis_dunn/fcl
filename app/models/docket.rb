class Docket
  include Mongoid::Document

  field :starts_at, type: Date
  field :scheduled_bill_ids, type: Array, default: -> { Array.new }
  field :sponsoring_legislator_ids, type: Array, default: -> { Array.new }
  index starts_at: -1

  class << self
    def latest
      all.sort(starts_at:-1).first
    end

    def upcoming
      where(:starts_at.gte => Time.now).sort(starts_at:-1).first
    end
  end

  def ends_at
    starts_at + 1.week
  end

  def period
    starts_at.strftime("%-m/%e") << " to " << ends_at.strftime("%-m/%e")
  end

  def bills(legislator_ids=nil)
    if legislator_ids
      criteria = Bill.where(id: { '$in' => scheduled_bill_ids }).all
      criteria.select do |bill|
        legislator_ids.include?(bill.sponsor.id) ||
        !(legislator_ids & bill.cosponsor_ids).empty?
      end
    else
      Bill.where id: { '$in' => scheduled_bill_ids }
    end
  end
end