class Draft
  include Mongoid::Document

  field :ordinal, type: String

  belongs_to :legislator
  belongs_to :user

  embedded_in :league

  before_validation :set_ordinal

  private

    def set_ordinal
      self.ordinal ||= league ? league.draft.size : 1
    end
end