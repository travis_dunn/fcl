class Voting
  include Mongoid::Document

  field :name, type: String
  field :choice, type: String
  field :party, type: String

  belongs_to :legislator, foreign_key: :_id

  embedded_in :vote

  validates :choice, presence: true
  validates :party, presence: true

  before_create :set_name

  def yea?; /y/i.match(choice[0]); end
  def nay?; /n/i.match(choice[0]); end
  def absent?; 'absent' == choice; end

  private

    def set_name
      self.name = legislator && legislator.display_name
    end
end