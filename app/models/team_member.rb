class TeamMember
  include Mongoid::Document

  field :name, type: String
  field :points, type: Integer, default: 0
  field :active, type: Boolean, default: false

  belongs_to :legislator, foreign_key: :_id

  embedded_in :team

  before_create :set_name

  alias_method :active?, :active

  def leader?
    team.leader_id == id
  end

  def legislator_name
    (name || '').split('[').first
  end

  def whip?
    team.whip_id == id
  end

  def activate
    self.active = true
  end

  def senator?; legislator.senator?; end
  def representative?; legislator.representative?; end

  private

    def set_name
      self.name = legislator && legislator.display_name
    end
end