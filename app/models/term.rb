class Term
  include Mongoid::Document

  TERM_TYPES = %w(rep sen)

  field :type, type: String
  field :party, type: String
  field :phone, type: String
  field :address, type: String
  field :state, type: String
  field :state_rank, type: String
  field :senate_class, type: Integer
  field :district, type: Integer
  field :points, type: Integer, default: 0
  field :starts_at, type: Date
  field :ends_at, type: Date

  embedded_in :legislator

  def <=>(other)
    starts_at <=> other.starts_at
  end    

  def senator?; type.match(/sen/); end

  def display_name
    if senator?
      "Senator Class #{senate_class} (#{duration})"
    else
      "Representative District #{district} (#{duration})"
    end
  end

  def duration
    "#{starts_at} - #{ends_at}"
  end

  def chamber
    type == 'sen' ? 'senate' : 'house'
  end
end