module Validations
  extend ActiveSupport::Concern

  private

    def set_error_messages_for(association_key)
      if not self.errors[association_key].nil?
        self.send(association_key).errors.each{ |attr,msg| self.errors.add(attr, msg)}
        self.errors.delete(association_key)
      end
    end
end