module Drafting
  extend ActiveSupport::Concern

  included do
    scope :drafting_deadlined, where(state: 'drafting', draft_type: {'$ne' => 'autopick'}, deadline: {'$ne' => nil} )
  end

  def drafting_deadlined?
    draft_deadline && Time.now - draft_deadline.hours >= last_updated
  end

  def draft_round
    if can_play?
      draft_rounds
    else
      (draft.size / slots).ceil
    end
  end

  def draft_rounds; formation.size; end
  def draft_turns; slots * formation.size; end

  def can_draft?
    teams.size == slots
  end

  def can_play?
    draft.size == draft_turns
  end 

  def current_drafting_team
    teams.detect{ |t| t.draft_ordinal == draft_ordinal }
  end 

  def available_legislators_for_drafting
    return [] unless drafting? && !can_play?

    legislators = if draft_round <= formation.senators
      Legislator.senators
    else
      Legislator.representatives
    end
    legislators = legislators.where(_id: { '$nin' => legislator_ids})
  end 

  protected

    def start_drafting?
      state_changed? && state_was == 'open'
    end

    def run_autodraft?
      Rails.logger.info "run_autodraft ::: draft.empty?=#{draft.empty?} && autodraft?=#{autodraft?} && start_drafting?=#{start_drafting?}"
      draft.empty? && autodraft? && start_drafting?
    end

    def draft_ordinal
      ordinal = draft.size % slots

      if draft_type == 'snake' && (draft_round%2 == 0)
        (slots-1) - ordinal
      else
        ordinal
      end
    end  

    def set_draft_ordinals
      ordinals = (0..(slots-1)).to_a
      teams.each do |team|
        ordinal = ordinals.delete_at( rand(ordinals.size) )
        team.update_attribute :draft_ordinal, ordinal
      end
    end

    def shift_draft_ordinals(from_ordinal)
      teams.where(draft_ordinal: {'$gt' => from_ordinal} )
           .find_and_modify('$inc' => {draft_ordinal: -1})
    end

  private
  
    def senator_draft_rounds; formation.senators; end
    def representative_draft_rounds; formation.representatives; end
 
    def available_to_draft?(legislator)
      !legislator_ids.include?(legislator)
    end

    def autodraft!
      autodraft_rounds senator_draft_rounds, Legislator.senators
      autodraft_rounds representative_draft_rounds, Legislator.representatives

      teams.each{|t| t.save! }
      self.state = 'playing'
      save

      set_rank_for_teams
    end

    def autodraft_rounds(rounds, legislators)
      turns = teams.size
      legislators = legislators.to_a.sort{|x,y| y.points <=> x.points }
      rounds.times do |r|
        turns.times do |t|
          pool = legislators.select{|l| l.points == legislators.first.points }
          legislator = pool[Random.rand(pool.size)]
          legislators.delete legislator

          puts "draft_ordinal=#{draft_ordinal} : r #{r} t #{t}"

          draft.build legislator: legislator
          current_drafting_team.members.build legislator: legislator 
        end
      end
    end
end