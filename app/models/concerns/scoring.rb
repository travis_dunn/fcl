module Scoring
  extend ActiveSupport::Concern

  included do
    has_many :scoresheets, as: :parent, dependent: :destroy
    has_one  :scoresheet, as: :parent

    after_initialize :create_scoresheet 
    before_create    :create_scoresheet 
  end
end