class Legislator
  include Mongoid::Document
  include Scoring

  field :current, type: Boolean, default: true
  field :cspan_id, type: String
  field :open_secrets_id, type: String
  field :gov_track_id, type: String
  field :votesmart_id, type: String
  field :thomas_id, type: String
  field :twitter_id, type: String
  field :youtube_id, type: String
  field :facebook_id, type: String
  field :last_name, type: String
  field :first_name, type: String
  field :middle_name, type: String
  field :suffix, type: String
  field :title, type: String
  field :description, type: String
  field :gender, type: String
  field :website, type: String
  field :gender, type: String
  field :religion, type: String
  field :birthday, type: Date
  field :attendance, type: Float, default: 100.0

  validates :thomas_id, presence: true

  embeds_many :terms

  has_many :annals

  scope :current, where('current' => true)
  scope :senators, -> { where('terms' => { '$elemMatch' => {'type' => 'sen', 'ends_at' => {'$gte' => Date.today }}} )}
  scope :representatives, -> { where( 'terms' => { '$elemMatch' => {'type' => 'rep', 'ends_at' => {'$gte' => Date.today }}} )}
  scope :veterans, where("this.terms.length > 1")
  scope :rookies, where("this.terms.length == 1")

  index 'terms.type' => 1, 'terms.ends_at' => -1, '_id' => 1
  
  def votes
    Vote.where('voting.legislator_id' => id)
  end

  def sponsored_bills
    Bill.where('sponsor._id' => id).all
  end

  def photo(width=100)
    "http://www.govtrack.us/data/photos/#{gov_track_id}-#{width}px.jpeg" unless gov_track_id.blank?
  end

  def party; term && term.party; end
  def party_abbr; party && term.party[0].upcase; end
  def chamber; term && term.chamber; end
  def chamber_abbr; term && term.chamber[0].upcase; end
  def state; term && term.state; end
  def office; term.senator? ? 'sen.' : 'rep.'; end
  def senator?; office[0] == 's'; end
  def representative?; office[0] == 'r'; end

  def term
    terms.sort{|x,y| y.starts_at <=> x.starts_at }.first
  end

  def committee_memberships
    Committee.where('members._id' => id)
             .map(&:members)
             .flatten
             .select{|m|m._id == id}
  end

  def legislative_activity
    annals.sort{|x,y| y.created_at <=> x.created_at }
  end

  def full_name
    [first_name,middle_name,last_name].select{|n|!n.blank?}.join(' ')
  end

  def display_name
    "#{full_name} [#{party}-#{state}]"
  end

  def short_title
    title && "#{title[0..2]}."
  end

  def in_draft?(drafts)
    drafts.detect{|d| d.legislator_id == id}
  end

  def points
    (scoresheet && scoresheet.points) || 0
  end

  private

    def create_scoresheet
      self.scoresheet ||= Scoresheet.new if new_record?
    end
end