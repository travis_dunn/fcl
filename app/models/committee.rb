class Committee
  include Mongoid::Document

  CATEGORIES = %w(house senate joint select)

  field :name, type: String
  field :category, type: String
  field :url, type: String
  field :congress, type: Integer, default: 113

  embeds_many :members, class_name: 'CommitteeMember', cascade_callbacks: true
  index 'members._id' => 1

  embeds_many :subcommittees

  alias_method :display_name, :name

  def bills
    Bill.where 'referrals._id' => _id.downcase
  end

  def officers
    members.select{|m| m.officer? }
  end

  def democrats
    members.select{|m| m.name.include? 'Democrat' }
  end

  def republicans
    members.select{|m| m.name.include? 'Republican' }
  end

  def senate?; category == 'senate'; end
  def house?; category == 'house'; end
end