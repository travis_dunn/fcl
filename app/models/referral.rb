class Referral
  include Mongoid::Document

  field :name, type: String
  field :activity, type: Array

  belongs_to :committee, foreign_key: :_id

  embedded_in :bill

  before_create :set_name

  validates :_id, presence: true

  private

    def set_name
      self.name = committee && committee.name
    end
end