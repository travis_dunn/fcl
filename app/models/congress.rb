class Congress
  include Mongoid::Document

  field :starts_at, type: Date
  field :ends_at, type: Date

  alias_method :number, :_id

  class << self
    def current
      new number: 113, starts_at: Date.new(2013, 1, 3), ends_at: Date.new(2015, 1, 3)
    end

    def historic
      Array.new
    end
  end

  def name
    "#{number.ordinalize} US Congress"
  end
end