class Subcommittee
  include Mongoid::Document

  field :name, type: String

  embedded_in :committee
end