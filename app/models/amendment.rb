class Amendment
  include Mongoid::Document

  field :description, type: String
  field :purpose, type: String
  field :chamber, type: String
  field :number, type: String
  field :category, type: String
  field :status, type: String
  field :introduced_at, type: Date
  field :updated_at, type: Date

  belongs_to :bill, index: true
  belongs_to :parent, class_name: 'Amendment'

  embeds_one :sponsor, cascade_callbacks: true
  embeds_many :actions, as: :legislation, cascade_callbacks: true

  after_create :track

  def display_name
    bill && (bill.display_name << ' Amendment')
  end  

  private

    def track
      if sponsor
        a = Annal.where(parent_id: self.id, parent_type: 'Amendment', legislator_id: sponsor.id, category: 'introduced_amendment').first_or_initialize
        a.created_at = introduced_at
        a.save

        Scoresheet.count! sponsor.id, :sponsored_amendments, introduced_at
      end
    end
end