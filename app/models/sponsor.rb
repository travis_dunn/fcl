class Sponsor
  include Mongoid::Document

  field :name, type: String
  field :sponsored_at, type: Date
  field :withdrawn_at, type: Date

  belongs_to :legislator, foreign_key: :_id

  embedded_in :bill

  before_create :set_name

  validates :_id, presence: true

  def withdrawn?
    !withdrawn_at.blank?
  end

  private

    def set_name
      self.name = legislator && legislator.display_name
    end
end