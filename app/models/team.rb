class Team
  include Mongoid::Document
  include Scoring

  DEFAULT_RANK = 1024

  field :name, type: String
  field :draft_ordinal, type: Integer
  field :active, type: Boolean, default: true
  field :rank, type: Integer, default: Team::DEFAULT_RANK

  belongs_to :leader, class_name: 'Legislator', index: true
  belongs_to :whip, class_name: 'Legislator'
  belongs_to :user, index: true
  belongs_to :league, index: true
  embeds_many :members, class_name: 'TeamMember', cascade_callbacks: true
  index 'members._id' => 1

  has_many :tradings
  has_many :trades

  scope :legal, where(league_id: {'$ne' => nil })

  validates :name, presence: true, length: {maximum: 128}
  validates :league_id, presence: true
  validate :validate_league, on: :create
  validate :validate_player_bans, on: :create

  alias_method :display_name, :name

  class << self
    def rank!
      League.playing.all.each do |league|
        teams = league.teams
        teams.sort{|x,y| y.points <=> x.points }.each_with_index do |t, idx|
          t.update_attribute(:rank, idx+1)
        end
      end
    end
  end

  def ranked?; rank != Team::DEFAULT_RANK; end

  def senators
    members.includes(:legislator).select{|m| m.senator? }
  end

  def representatives
    members.includes(:legislator).select{|m| m.representative? }
  end  

  def standing_rank
    rank == 1024 ? 'Unranked' : rank.ordinalize
  end

  def leader
    members.detect{|m| m.leader? }
  end

  def whip
    members.detect{|m| m.whip? }
  end

  def active_members
    members.select{|m| m.active? }
  end

  def ranked?
    points > 0 && rank > 0 && rank != 1024
  end

  def points
    members.map{|m| m.points.to_i}.inject(:+) || 0
  end

  def legislator_points
    @legislator_points ||= (members.map{|m| m.legislator.points.to_i}.inject(:+) || 0)
  end

  def legislator_ids
    members.map(&:_id)
  end

  def roster_full?
    active_members.size >= league.formation.roster
  end

  def can_recruit?
    members.size < league.formation.size
  end

  def activate!(team_member_id)
    unless roster_full?
      if member = get_member(team_member_id)
        member.active = true
        save
      end
    end
  end

  def recess!(team_member_id)
    if member = get_member(team_member_id)
      self.leader_id = nil if member.leader?
      self.whip_id = nil if member.whip?
      member.active = false
      save
    end
  end

  def leader!(team_member_id)
    if member = get_member(team_member_id)
      self.leader_id = member._id
      save
    end
  end

  def whip!(team_member_id)
    if member = get_member(team_member_id)
      members.each {|m| m.position = nil }
      self.whip_id = member.legislator_id
      save
    end
  end

  def retire!(team_member_id)
    if member = get_member(team_member_id)
      members.delete(member)
      save
    end
  end

  def recruit!(legislator_id)
    if legislator = league.recruits.where(_id: legislator_id).first
      members.build legislator: legislator 
      save
    end
  end

  def annals
    @activity ||= Annal.where(legislator_id: {'$in' => legislator_ids})
                       .sort(created_at:-1)
  end

  def peers
    league.teams.where(rank: {'$gte' => rank-2, '$lte' => rank+2 }).sort(rank:1)
  end

  def get_member(team_member_id)
    members.detect{|m| m.id.to_s == team_member_id.to_s }
  end

  private

    def validate_player_bans
      errors.add :user_id, "banned from league" if league && league.player_banned?(user_id)
    end

    def validate_league
      errors.add :league_id, "not open to join" if league && !league.open?
    end
end