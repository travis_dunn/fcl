class Annal
  include Mongoid::Document

  CATEGORIES = %w(
    defeated_bill 
    introduced_amendment 
    passed_amendment
    sponsored_legislation
    sponsored_referred_legislation 
    sponsored_passed_legislation 
    sponsored_approved_legislation 
    sponsored_enacted_legislation 
    cosponsored_enacted_legislation 
    maverick_vote
    invoked_cloture
    rejected_cloture
  )

  field :name, type: String  
  field :category, type: String  
  field :points, type: Integer, default: 0
  field :congress, type: Integer, default: 113
  field :created_at, type: Date
  index created_at: -1

  belongs_to :parent, polymorphic: true, index: true
  belongs_to :legislator, index: true

  before_create :set_name
  before_create :set_points
  after_create :award_points

  validates :legislator_id, presence: true

  def description
    category.humanize
  end

  def bill_id
    parent.respond_to?(:bill_id) ? parent.bill_id : parent
  end

  private

    def set_name
      self.name = self.parent && self.parent.display_name
    end

    def set_points
      self.points = if category == 'defeated_bill'
        5
      elsif category == 'introduced_amendment'
        5
      elsif category == 'passed_amendment'
        20
      elsif category == 'sponsored_legislation'
        5
      elsif category == 'sponsored_referred_legislation'
        15
      elsif category == 'sponsored_passed_legislation'
        20
      elsif category == 'sponsored_approved_legislation'
        15
      elsif category == 'sponsored_enacted_legislation'
        50
      elsif category == 'cosponsored_enacted_legislation'
        20
      elsif category == 'maverick_vote'
        5
      elsif category == 'invoked_cloture'
        5
      elsif category == 'rejected_cloture'
        5
      else
        0
      end
    end

    def award_points
      criteria = Team.where('active' => true, 'members._id' => legislator_id)

      award_teams(criteria)
      award_legislators(criteria)
    end  

    def award_teams(criteria)
      criteria.where('leader_id' => {'$ne' => legislator_id})
              .update_all('$inc' => {'members.$.points' => points})
    end

    def award_legislators(criteria)
      criteria.where('leader_id' => legislator_id)
              .update_all('$inc' => {'members.$.points' => points*2 })

      Scoresheet.count! legislator_id, :points, created_at, points
    end
end