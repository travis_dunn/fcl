
class League
  include Mongoid::Document
  include Drafting
  include Validations

  STATES = %w(open drafting playing concluded)
  DRAFT_TYPES = %w(snake linear autopick)
  TOURNAMENTS = %w(endurance round_robin elimination)
  SEASONS = %w(congress session)
  ROSTERING_TYPES = %w(always evenings weekends)
  MIN_SLOTS = 2
  MAX_SLOTS = 25

  field :name, type: String
  field :description, type: String
  field :season, type: String, default: 'session'
  field :state, type: String, default: 'open'
  field :draft_type, type: String, default: 'snake'
  field :rostering_type, type: String, default: ROSTERING_TYPES.first
  field :draft_deadline, type: Integer, default: 8
  field :tournament, type: String, default: 'swiss'
  field :slots, type: Integer, default: 8
  field :congress, type: Integer, default: 113
  field :banned, type: Array
  field :created_at, type: Date, default: -> { Time.now.utc }
  field :password, type: String, default: nil
  validates :name, presence: true, length: {maximum: 128}
  validates :slots, numericality: { greater_than_or_equal_to: MIN_SLOTS, less_than_or_equal_to: MAX_SLOTS }
  validates :formation, presence: true
  validates :state, inclusion: { in: STATES }
  validates :rostering_type, inclusion: { in: ROSTERING_TYPES }
  validates :draft_type, inclusion: { in: DRAFT_TYPES }
  validates :season, inclusion: { in: SEASONS }
  validates_associated :formation

  has_many :teams, dependent: :destroy
  has_many :tradings, dependent: :destroy
  embeds_one  :formation, cascade_callbacks: true
  embeds_many :draft, class_name: 'Draft'

  accepts_nested_attributes_for :formation

  belongs_to :commissioner, class_name: 'User'

  scope :public, where(password: nil)
  scope :open, where(state: 'open')
  scope :playing, where(state: 'playing')
  scope :drafting, where(state: 'drafting')

  before_save   :set_password
  before_update :set_draft_ordinals, if: :start_drafting?
  after_update  :autodraft!, if: :run_autodraft?
  after_update  :activate_teams, if: :activate_teams?
  after_update  :set_rank_for_teams, if: :start_playing?
  after_validation :set_error_messages

  def legislator_ids
    teams.map(&:legislator_ids).flatten
  end

  def recruits
    playing? ? Legislator.where(:_id => {'$nin' => legislator_ids}) : Array.new
  end

  def player_ids
    teams.map(&:user_id)
  end

  def open?; state == 'open'; end
  def drafting?; state == 'drafting'; end
  def playing?; state == 'playing'; end
  def autodraft?; draft_type == 'autopick'; end
  def private?; !password.blank?; end

  def can_trade?; playing?; end
  def can_join?; open? && !can_draft?; end

  def activate_teams?
    Rails.logger.info "activate_teams: run_autodraft?=#{run_autodraft?} || start_playing?=#{start_playing?} ... can_play?=#{can_play?}" 
    start_playing?
  end

  def user_joined?(user_id)
    teams.map(&:user_id).include?(user_id)
  end

  def max_senators
    (100 / slots).floor
  end

  def max_representatives
    (400 / slots).floor
  end

  def roster_evenings?; rostering_type == 'evenings'; end
  def roster_weekends?; rostering_type == 'weekends'; end

  def can_manage_teams_now?
    if roster_evenings?
      Time.now.hour >= 18 # 7pm
    elsif roster_weekends?
      Time.now.wday == 0 || Time.now.wday == 6
    else
      true
    end
  end

  def ban_team!(team)
    return unless team.league_id == id

    team.update_attribute :league_id, nil
    shift_draft_ordinals team.draft_ordinal
    save
  end

  def player_banned?(user_id); banned && banned.include?(user_id); end

  def status
    if can_join?
      'Waiting for teams'
    elsif open? && can_draft?
      'Waiting to draft'
    elsif drafting?
      'Drafting'
    elsif playing?
      'Playing'
    else
      'Played'
    end
  end

  protected

    def start_playing?
      Rails.logger.info "start_playing: state_changed?=#{state_changed?} && state_was == 'drafting' =#{state_was} && can_play?=#{can_play?}" 
      state_changed? && state_was == 'drafting' && can_play?
    end

    def ban_player!(user_id)
      self.banned ||= []
      banned << user_id unless player_banned?(user_id)
    end

  private

    def set_password; self.password = nil if password.blank?; end

    def set_error_messages; set_error_messages_for :formation; end

    def activate_teams
      teams.each do |team|
        formation.senators.times {|i| team.senators[i].activate }
        formation.representatives.times {|i| team.representatives[i].activate }
        team.save
      end
    end

    def set_rank_for_teams
      Rails.logger.info "CALLED set_rank_for_teams"
      teams.sort{|x,y| x.legislator_points <=> y.legislator_points }.each_with_index do |team, idx|
        Rails.logger.info "points: #{team.legislator_points} for rank #{(idx+1)}"
        team.update_attribute(:rank, idx+1) unless team.ranked?
      end
    end
end