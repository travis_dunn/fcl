class Formation
  include Mongoid::Document

  MIN_SIZE = 12

  field :senators, type: Integer, default: 4
  field :representatives, type: Integer, default: 12
  field :roster, type: Integer, default: 10

  embedded_in :league

  validates :senators, numericality: { greater_than_or_equal_to: 0 }
  validates :representatives, numericality: { greater_than_or_equal_to: 0 }

  validate :validates_size

  def size
    senators + representatives
  end

  private

    def validates_size
      if senators == 0 && representatives < MIN_SIZE
        errors.add :representatives, "must be least #{MIN_SIZE}"
      elsif representatives == 0 && senators < MIN_SIZE
        errors.add :senators, "must be least #{MIN_SIZE}"
      end
      if senators > league.max_senators
        errors.add :senators, "must be less than or equal to #{league.max_senators}"
      end
      if representatives > league.max_representatives
        errors.add :representatives, "must be less than or equal to #{league.max_representatives}"
      end
    end
end