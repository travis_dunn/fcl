class Trade
  include Mongoid::Document

  belongs_to :team, index: true
  belongs_to :team_member, index: true
end