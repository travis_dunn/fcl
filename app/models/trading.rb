class Trading
  include Mongoid::Document

  belongs_to :league, index: true
  belongs_to :team, index: true
  belongs_to :team_member, index: true
  has_many   :trades, dependent: :destroy

  def legislator
    team.get_member team_member_id
  end
end