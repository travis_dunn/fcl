class CommitteeMember
  include Mongoid::Document

  TITLES = {
    'Chair' => 1,
    'Chairman' => 1,
    'Co-Chairman'  => 2,
    'Vice Chairman'  => 2,
    'Ranking Member'  => 3,
    'Ex Officio'  => 4,
    'Member'  => 10
  }

  field :name, type: String
  field :title, type: String, default: 'Member'

  belongs_to :legislator, foreign_key: :_id

  embedded_in :committee

  before_validation :set_name

  validates :name, presence: true

  def <=>(other)
    TITLES[title] <=> TITLES[other.title]
  end  

  def chair?; title == 'Chair' || title == 'Chairman'; end
  def officer?; title && title != 'Member'; end

  def committee_name
    committee && committee.name
  end

  private

    def set_name
      self.name ||= legislator && legislator.display_name
    end
end