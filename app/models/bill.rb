class Bill
  include Mongoid::Document
  include Mongoid::Timestamps::Updated

  field :name, type: String
  field :official_title, type: String
  field :category, type: String
  field :number, type: String
  field :status, type: String
  field :congress, type: Integer, default: 113
  field :subjects, type: Array
  field :introduced_at, type: Date
  field :summary, type: String
  field :related, type: Array

  embeds_one :sponsor, cascade_callbacks: true
  embeds_many :cosponsors, class_name: 'Sponsor', cascade_callbacks: true
  embeds_many :referrals, cascade_callbacks: true
  embeds_many :actions, as: :legislation, cascade_callbacks: true
  has_many :amendments
  index 'referrals._id' => 1

  after_create :annal

  scope :current, where(congress: 113)
  scope :house,   where(category: { '$in' => ['h','hr']})
  scope :senate,  where(category: { '$in' => ['s','sres']})
  scope :joint,   where(category: { '$in' => ['sconres','sjres','hconres','hjres']})
  scope :introduced, where(status: 'INTRODUCED')
  scope :referred, where(status: { '$in' => ['REFERRED','REPORTED']})
  scope :defeated, where(status: { '$in' => ['FAIL:ORIGINATING:HOUSE','FAIL:ORIGINATING:SENATE','PROV_KILL:SUSPENSIONFAILED','PROV_KILL:CLOTUREFAILED']})
  scope :enacted,  where(status: { '$in' => ['PASSED:CONCURRENTRES','ENACTED:SIGNED','PASSED:SIMPLERES','PASSED:BILL']})

  def short_title
    "#{category.upcase}-#{number}"
  end

  def display_name
    case category
    when 'hconres', 'sconres'
      'Concurrent Resolution ' << number
    when 'h'
      'House Bill ' << number
    when 's'
      'Senate Bill ' << number
    when 'sjres', 'hjres'
      'Joint Resolution ' << number
    when 'sres'
      'Senate Resolution ' << number
    when 'hr'
      'House Resolution ' << number
    else
      category.upcase
    end
  end

  def cosponsor_ids
    cosponsors.map(&:_id)
  end

  def status_description
    status
  end

  def related_bills
    @related_bills ||= if related.blank?
      Array.new
    else
      Bill.where(id: {'$in' => related})
    end
  end

  def last_action
    actions.last
  end

  private

    def annal
      if sponsor
        a = Annal.where(parent_id: self._id, parent_type: 'Bill', legislator_id: sponsor._id, category: 'sponsored_legislation').first_or_initialize
        a.created_at = introduced_at
        a.save

        Scoresheet.count! sponsor.id, :sponsored_bills, introduced_at
      end

      unless cosponsor_ids.blank?
        Scoresheet.count! cosponsor_ids, :cosponsored_bills, introduced_at
      end
    end
end