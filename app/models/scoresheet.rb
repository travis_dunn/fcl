class Scoresheet
  include Mongoid::Document

  field :defeated_bills, type: Integer, default: 0
  field :sponsored_bills, type: Integer, default: 0
  field :cosponsored_bills, type: Integer, default: 0
  field :sponsored_enacted_bills, type: Integer, default: 0
  field :cosponsored_enacted_bills, type: Integer, default: 0
  field :sponsored_amendments, type: Integer, default: 0
  field :passed_amendments, type: Integer, default: 0
  field :winning_cloture_votes, type: Integer, default: 0
  field :maverick_votes, type: Integer, default: 0
  field :points, type: Integer, default: 0
  field :absences, type: Integer, default: 0
  field :congress, type: Integer, default: 113
  field :created_at, type: Date

  index created_at: -1
  index parent_id: 1

  scope :total, -> { where(created_at: nil).first }
  scope :daily, -> { where(created_at: {'$ne' => nil}).desc(:created_at).first }
  
  belongs_to :parent, polymorphic: true, index: true

  class << self
    def count!(legislator_or_ids, field, date, value=1)
      if legislator_or_ids.is_a? Legislator
        count_legislator legislator_or_ids, field, date, value
        count_legislator legislator_or_ids, field, nil, value
      elsif legislator_or_ids.is_a?(Array)
        count_legislators legislator_or_ids, field, date, value
        count_legislators legislator_or_ids, field, nil, value
      else
        count_legislators [legislator_or_ids], field, date, value
        count_legislators [legislator_or_ids], field, nil, value
      end
    end

    private

      def count_legislator(legislator, field, date, value=1)
        criteria = {'$inc' => {"#{field}" => value}}

        legislator.scoresheets.where(created_at: date)
                  .find_and_modify(criteria, {upsert: true})        
      end

      def count_legislators(ids, field, date, value=1)
        criteria = {'$inc' => {"#{field}" => value}}
        ids.each do |id|
          where(parent_id: id, parent_type: 'Legislator', created_at: date)
               .find_and_modify(criteria, {upsert: true})        
        end
      end
  end  
end