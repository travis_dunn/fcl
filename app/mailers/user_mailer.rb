class UserMailer < BaseMailer
  def registered(email)
    mail :to => email,
         :from => sender,
         :subject => "Welcome to Fantasy Congress League"
  end

  def forgot_password(email, token)
    @token = token

    mail :to => email,
         :from => sender,
         :subject => "Fantasy Congress League: Password Recovery"
  end  
end