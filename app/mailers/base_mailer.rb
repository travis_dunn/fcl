class BaseMailer < ActionMailer::Base
  helper EmailHelper

  SENDER_EMAIL = 'test@example.com'

  def mail(opts)
    append_rails_view_path 'app/views/mailers'
    super opts
  end

  protected

    def sender
      "#{BaseMailer::SENDER_EMAIL} <#{BaseMailer::SENDER_EMAIL}>"
    end  

  private

    def append_rails_view_path(path)
      append_view_path Rails.root.join(path)
    end  
end