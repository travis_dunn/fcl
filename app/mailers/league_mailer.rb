class LeagueMailer < BaseMailer
  def team_joined(team_name, team_id, league_name, league_id, commissioner_email)
    @team_name = team_name
    @team_id = team_id
    @league_name = league_name
    @league_id = league_id

    mail :to => commissioner_email,
         :from => sender,
         :subject => "Fantasy Congress League: Team Joined Your League"    
  end

  def draft_ready(league_name, league_id, commissioner_email)
    @league_name = league_name
    @league_id = league_id

    mail :to => commissioner_email,
         :from => sender,
         :subject => "Fantasy Congress League: Draft Ready"        
  end

  def draft_finished(league_name, league_id, commissioner_email)
    @league_name = league_name
    @league_id = league_id

    mail :to => commissioner_email,
         :from => sender,
         :subject => "Fantasy Congress League: Draft Finished"        
  end  
end