class UsersController < ApplicationController
  before_filter :require_user, only: [:show]
  before_filter :require_player, only: [:edit, :update]

  def show
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.registration user_params
  	if @user.save
      current_user_login!(@user.id)
      UserMailer.delay.registered @user.email
      return_or_redirect_to leagues_path
  	else
      render :new
  	end
  end

  def edit
  end

  def update
    if @user.update_attributes( user_params )
      redirect_to edit_user_path
    else
      render :edit
    end
  end

  private

    def require_player
      redirect_to new_login_path unless @user = current_user
    end

    def require_user
      redirect_to root_path unless @user = User.where(id: params[:id]).first
    end

    def user_params
      params.fetch(:user, {})
            .permit(:username, :email, :password)
    end  
end