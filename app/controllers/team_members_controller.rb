class TeamMembersController < ApplicationController
  before_filter :require_league
  before_filter :require_team

  def new
  	@team_member = TeamMember.new
    @legislators = @league.available_legislators_for_drafting.includes(:scoresheet)
    @legislators = @legislators.to_a.sort{|x,y| y.scoresheet.points <=> x.scoresheet.points }

  end

  def create
    @league.draft.build draft_params
  	@team.members.build(team_member_params) if @league.valid?
    
  	if @team.save && @league.save
      if @league.can_play?
        LeagueMailer.delay.draft_finished(league_name, league_id, commissioner_email) 
      else
      end

      redirect_to league_team_path(@league, @team)
  	else
      @legislators = current_legislators.all
      render :new
  	end
  end

  def update
    call_rpc! params[:rpc], params[:id]
    redirect_to edit_league_team_path(@league, @team)
  end

  def destroy
    call_rpc! params[:rpc], params[:id]
    redirect_to edit_league_team_path(@league, @team)
  end  

  private

    def call_rpc!(rpc, member_id)
      return unless @team.league.can_manage_teams_now?
      
      case rpc
      when 'activate'
        @team.activate! member_id
      when 'leader'
        @team.leader! member_id
      when 'whip'
        @team.whip! member_id
      when 'vacate'
        @team.vacate! member_id
      when 'recess'
        @team.recess! member_id
      when 'retire'
        @team.retire! member_id
      when 'recruit'
        @team.recruit! member_id
      end
    end

    def require_league
      redirect_to root_path unless load_league(params[:league_id])
    end

    def require_team
      redirect_to root_path unless load_team(params[:team_id])
    end

    def draft_params
      team_member_params
    end

    def team_member_params
      params.fetch(:team_member, {})
            .permit(:_id)
    end  
end