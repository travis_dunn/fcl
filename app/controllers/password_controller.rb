class PasswordController < ApplicationController
  before_filter :recover_user, only: [:edit, :update]

  def new
    @user = User.new
  end

  def create
    @user = User.find_by_email user_params[:email]
    if @user
      @user.generate_login_token! 
      UserMailer.delay.forgot_password @user.email, @user.login_token
    end
    redirect_to login_password_path('sent')
  end

  def show
  end

  def edit
  end

  def update
    @user.password = user_params.fetch(:password, nil)
    @user.login_token = nil

    if @user.save
      current_user_login!(@user.id)

      redirect_to capital_hill_path
    else
      render :edit
    end
  end

  private

    def recover_user
      @user = User.recover params[:id]
    end

    def user_params
      params.fetch(:user, {})
            .permit(:email, :password, :password_confirmation)
    end  
end