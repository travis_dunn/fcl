class LegislatorsController < ApplicationController
  include Pagination
  
  before_filter :require_legislator, only: [:show]
  before_filter :load_legislators, only: [:index]

  def show
  	@membership = @legislator.committee_memberships
  	@annals = @legislator.legislative_activity[0..5]
    @bills = @legislator.sponsored_bills
  end

  def index
  end

  private

    def require_legislator
      redirect_back unless @legislator = Legislator.where(id: params[:id]).first
    end

    def load_legislators
      @legislators = Legislator.includes(:scoresheet).current
      @legislators = @legislators.where(gender: /^#{params[:gender]}/i) if params[:gender]
      @legislators = @legislators.where(last_name: /^#{params[:alpha]}/i) if params[:alpha]
      @legislators = @legislators.where('terms.type' => params[:chamber]) if params[:chamber]
      @legislators = @legislators.where('terms.party' => params[:party].titleize) if params[:party]
      if params[:status] == 'veteran'
        @legislators = @legislators.veterans
      elsif params[:status] == 'rookie'
        @legislators = @legislators.rookies
      end
      @legislators = paginate @legislators
    end
end