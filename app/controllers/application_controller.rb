class ApplicationController < ActionController::Base
  include CurrentUser
  include Navigation
  
  protect_from_forgery with: :exception

  protected

    def load_league(id)
      @league = League.where(id: id).first if id
    end

    def load_team(id)
      @team = Team.where(id: id).first if id
    end    

    def load_user_team(id)
      @team = current_user.teams.where(id: id).first if id
    end        

    def load_legislator(id)
      @legislator = Legislator.where(id: id).first if id
    end    
end
