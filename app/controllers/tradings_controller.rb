class TradingsController < ApplicationController
  before_filter :require_league
  before_filter :require_team

  def create
    @team.tradings.create trading_params
    redirect_to edit_league_team_path(@league, @team)
  end

  private
  
    def require_league
      redirect_to root_path unless load_league(params[:league_id])
    end

    def require_team
      redirect_to root_path unless load_user_team(params[:team_id])
    end

    def require_team_member
      return if @team_member = @team.get_member(params[:id])
      redirect_to league_team_path(@league, @team)
    end

    def trading_params
      params.fetch(:trading, {})
            .permit(:team_member_id)
            .merge(league: @league)
    end      
end