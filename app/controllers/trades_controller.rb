class TradesController < ApplicationController
  before_filter :require_league
  before_filter :require_team

  private
  
    def require_league
      redirect_to root_path unless load_league(params[:league_id])
    end

    def require_team
      redirect_to root_path unless load_team(params[:team_id])
    end
end