class TeamsController < ApplicationController
  include Privacy
  
  before_filter :require_league
  before_filter :require_open_league, only: [:new, :create]
  before_filter :require_team, only: [:show, :edit, :update, :destroy]
  before_filter :require_comissioner, only: [:destroy]

  def new
  	@team = Team.new
  end

  def create
    @team = current_user.teams.new team_params
    if @team.save
      LeagueMailer.delay.team_joined(@team.name, @team.id, @league.name, @league.id, @league.commissioner.email)
      LeagueMailer.delay.draft_ready(@league.name, @league.id, @league.commissioner.email) if @league.can_draft?

      redirect_to league_path(@league)
  	else
      render :new
    end
  end

  def show
    @activity = @team.annals.limit(10)
    @docket = Docket.latest.bills(@team.legislator_ids)
    @teams = @team.peers.sort{|x,y| y.points <=> x.points }
  end

  def edit
    load_team_details
  end

  def update
    if @team.update_attributes(team_params)
      redirect_to edit_league_team_path(@league, @team)
    else
      load_team_details
      render :edit
    end
  end

  def destroy
    @league.ban_team!(@team)
    redirect_to edit_league_path(@league)
  end

  private

    def require_league
      redirect_to root_path unless load_league(params[:league_id])
    end

    def require_open_league
      redirect_to league_path(@league) unless @league.can_join?
    end

    def require_team
      redirect_to league_path(@league) unless load_team(params[:id])
    end

    def require_comissioner
      redirect_to root_path unless @league.commissioner_id == current_user_id
    end

    def load_team_details
      @recruits = @league.recruits if @team.can_recruit?

      if @league.can_trade?
        @league_tradings = @league.tradings.where(team_id: {'$ne' => @team.id})
        @team_tradings = @team.tradings
        @team_trades = @team.trades
        @trading = Trading.new
      end
    end

    def team_params
      params.fetch(:team, {})
            .permit(:name)
            .merge(league: @league)
    end  
end