class LoginController < ApplicationController
  before_filter :current_user_logout!, :only => [:create, :destroy]

  def new
    @user = User.new
  end

  def create
  	if @user = User.login(login_params[:email], login_params[:password])
      current_user_login!(@user.id)
      return_or_redirect_to capital_hill_path
  	else
      @user = User.new
      @user.errors[:base] << "Incorrect username or password"
      render :new
  	end
  end

  def destroy
    redirect_to root_path
  end

  private

    def login_params
      params.fetch(:user, {})
            .permit(:email, :password)
    end  
end