module Privacy
  extend ActiveSupport::Concern

  included do
    helper_method :is_public_or_authenticated_league?
  end

  def is_public_or_authenticated_league?
    @league && (!@league.private? || has_authenticated_league?) ? true : false
  end

  private

    def require_league_privacy
      redirect_to leagues_path unless is_public_or_authenticated_league?
    end

    def has_authenticated_league?
      authenticated_private_leagues.include?(@league.id.to_s) ||
      current_user_id == @league.commissioner_id ||
      @league.player_ids.include?(current_user_id)
    end

    def authenticated_private_leagues
      (session[:private_league_ids] || '').split(',')
    end

    def authenticate_private_league(league_id)
      return if authenticated_private_leagues.include?(league_id.to_s)
      session[:private_league_ids] = (authenticated_private_leagues + [league_id.to_s]).join(',')
    end
end