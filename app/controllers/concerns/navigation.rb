module Navigation
  extend ActiveSupport::Concern

  protected

    def redirect_back
      redirect_to back_path
    end

    def back_path
      request.env["HTTP_REFERER"] ? :back : root_path
    end

    def store_return_to(path=nil)
      session[:return_to] = path || request.path
    end

    def return_or_redirect_to(url)
      url = session.delete(:return_to) if return_to_url_stored?
      redirect_to url
    end

  private

    def return_to_url_stored?
      session[:return_to] ? true : false
    end
end