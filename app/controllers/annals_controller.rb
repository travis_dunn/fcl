class AnnalsController < ApplicationController
  include Pagination

  before_filter :require_parent
  before_filter :load_annals

  def index
  end

  def show
    render :index 
  end

  private

    def load_annals
      @annals = paginate @parent.annals
    end

    def require_parent
      redirect_back unless load_parent
    end

    def load_parent
      @parent = if load_team(params[:team_id]) && load_league(params[:league_id])
        @team
      elsif load_legislator(params[:legislator_id])
        @legislator
      end
    end 
end