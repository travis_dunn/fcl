class BillsController < ApplicationController
  include Pagination

  before_filter :require_bill, only: [:show]
  before_filter :load_bills, only: [:index]

  def show
  end

  def index
    Rails.logger.info Bill.distinct(:status)
  end

  private

    def require_bill
      redirect_back unless @bill = Bill.where(id: params[:id]).first
    end

    def load_bills
      @bills = if params[:chamber]
        case params[:chamber]
        when 'house'
          paginate Bill.current.house
        when 'senate'
          paginate Bill.current.senate
        when 'joint'
          paginate Bill.current.joint
        else
          paginate Bill.current
        end
      elsif params[:status]
        case params[:status]
        when 'introduced'
          paginate Bill.current.introduced
        when 'referred'
          paginate Bill.current.referred
        when 'enacted'
          paginate Bill.current.enacted
        when 'defeated'
          paginate Bill.current.defeated
        else
        end
      elsif params[:docket]
        docket = Docket.latest || Docket.new
        docket.bills
      else
        paginate Bill.current
      end
    end
end