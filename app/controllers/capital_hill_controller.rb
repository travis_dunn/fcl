class CapitalHillController < ApplicationController
  before_filter :require_current_user

  def show
  	@teams = current_user.teams.legal.includes(:league)
    @leagues = League.where commissioner_id: current_user_id
  end
end