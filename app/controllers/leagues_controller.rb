class LeaguesController < ApplicationController
  include Privacy

  before_filter :require_current_user, only: [:new, :create]
  before_filter :require_league, only: [:show, :edit, :update, :authenticate]
  before_filter :require_commissioner, only: [:edit, :update]

  def index
    @open_leagues = League.open.public.all
    @active_leagues = League.playing.public.all
  end

  def new
  	@league = League.new(formation: Formation.new)
  end

  def create
  	@league = current_user.leagues.new league_params

  	if @league.save
      redirect_to new_league_team_path(@league)
  	else
      render :new
  	end
  end

  def show
    @teams = sorted_league_teams @league
  end

  def edit
  end

  def update
    if @league.update_attributes(league_params)
      redirect_to league_path(@league)
    else
      render :edit
    end
  end

  def authenticate
    password = authenticate_params[:password]
    if @league.authenticate(password)
      authenticate_private_league(@league.id) 
      redirect_to league_path(@league)
    else
      @league.errors.add :password, 'is incorrect'
      render :show
    end
  end

  private

    def sorted_league_teams(league)
      teams = league.teams.to_a
      if league.drafting?
        teams = teams.sort{|x,y| puts "#{x.draft_ordinal} <=> #{y.draft_ordinal}" ;x.draft_ordinal <=> y.draft_ordinal }
      else
        teams = teams.sort{|x,y| x.rank <=> y.rank }
      end
    end

    def require_league
      redirect_to root_path unless load_league(params[:id])
    end

    def require_commissioner
      redirect_to back_path unless current_user_id == @league.commissioner_id
    end

    def league_params
      params.fetch(:league, {})
            .permit(:name, :password, :state, :draft_type, :rostering_type, :tournament, :season, :slots, :description, formation_attributes: [:senators, :representatives] )
    end

    def authenticate_params
      params.fetch(:league, {}).permit(:password)
    end
end