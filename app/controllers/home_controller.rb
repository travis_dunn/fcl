class HomeController < ApplicationController
  def show; end

  def help; end

  def statistics
    @bill_count = Bill.count
  end
end