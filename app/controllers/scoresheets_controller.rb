class ScoresheetsController < ApplicationController
  include Pagination

  before_filter :require_league
  before_filter :require_parent
  before_filter :load_scoresheets

  def index
  end

  private

    def load_scoresheets
      @scoresheets = paginate @parent.scoresheets
    end

    def require_parent
      redirect_back unless load_parent
    end
end