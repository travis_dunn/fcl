class CommitteesController < ApplicationController
  before_filter :require_committee, only: [:show]
  before_filter :load_committees, only: [:index]

  def show
  end

  def index
  end

  private

    def require_committee
      redirect_back unless @committee = Committee.where(id: params[:id]).first
      @bills = @committee.bills
      @members = @committee.members.sort{|x,y| x <=> y }
      @subcommittees = @committee.subcommittees
    end

    def load_committees
      @committees = Committee.all
      @senate = @committees.select{|c| c.senate? }
      @house = @committees.select{|c| c.house? }
      @joint = @committees.select{|c| not c.house? and not c.senate? }
    end    
end